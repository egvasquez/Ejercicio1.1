package Principal;

import Clases.Deporte;
import Clases.Deportista;
import Clases.Especialista;
import Clases.Ministerio;

import java.util.InputMismatchException;
import java.util.Scanner;


public class ConsoleApp {

    public static void main(String[] args) throws Exception {


        Ministerio ministerio = null;
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir) {

            System.out.println("1. Ingresar cantidad de deportes Ministerio:--");
            System.out.println("2. Datos del Especialista asignado:--");
            System.out.println("3. Adicionar deportes:--");
            System.out.println("4. Listar deportes:--");
            System.out.println("5. Adicionar Deportista:--");
            System.out.println("6. Listar Deportista promedio de edad:--");
            System.out.println("7. Salir");

            System.out.println("Sistema del Ministerio de Deportes");

            try {
                System.out.println("Seleccione una de las opciones");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:
                        System.out.println("Cuantos deportes posee el Ministerio:--");
                        ministerio = new Ministerio(sn.nextInt());
                        break;
                    case 2:
                        System.out.println("Nombre:--");
                        Especialista especialista = new Especialista();
                        especialista.setNombre(sn.next());
                        System.out.println("Años de experiencia:--");
                        especialista.setAnnosExp(sn.nextInt());
                        break;
                    case 3:
                        System.out.println("Nombre del deporte:--");
                        Deporte deporte = new Deporte(sn.next());
                        System.out.println("Con balón? (S/N):");
                        if (sn.next().equalsIgnoreCase("S"))
                            deporte.setConBalon(true);
                        ministerio.adicionarDeporte(deporte);

                        break;
                    case 4:
                        Deporte[] listado = ministerio.getListado();
                        for (int i = 0; i <ministerio.getCantReal() ; i++) {
                            if(listado[i].isConBalon())
                                System.out.printf("Deporte #"+(i+1)+ ": "+listado[i].getNombre()+"\n");
                        }
                        System.out.println("--------------");
                        break;
                    case 5:
                        System.out.println("ingres nombre deportista:__");
                        Deportista dep =new Deportista();
                        dep.setNombre(sn.next());
                        System.out.println("ingrese edad Deportista:__");
                        dep.setEdad(sn.nextInt());
                        ministerio.addDeportista(dep);
                        break;

                    case 6:
                        Deportista[] listas =ministerio.getListas();
                        for (int j=0;j<ministerio.getCantReal();j++){
                            System.out.println("deportista #"+(j+1)+":"+listas[j].getNombre()+"\n");


                        }


                        break;

                    case 7:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 5");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            }
        }

    }
}
